package com.github.simplestep.kafka.tutorial1;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class ProducerDemo {

    public static void main(String[] args) {
        System.out.println("Start Kafka Producer Demo.");

        //create Producer properties

        Properties  properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"127.0.0.1:9092");
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,StringSerializer.class.getName());

        //create the producer
        KafkaProducer<String,String> producer = new KafkaProducer<String, String>(properties);
        sendSomeKafkaRecords(producer);


        producer.flush();
        producer.close();

    }

    private static void sendSomeKafkaRecords(KafkaProducer<String, String> producer) {
        //create a producer record
        for(int i=100;i>0;i--) {
            ProducerRecord<String, String> record = new ProducerRecord<>("first_topic", i +" bottles of beer.");
            //send data
            producer.send(record);
        }
    }
}
