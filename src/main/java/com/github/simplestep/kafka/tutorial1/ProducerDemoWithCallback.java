package com.github.simplestep.kafka.tutorial1;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;



public class ProducerDemoWithCallback {

    public static void main(String[] args) {
        System.out.println("Start Producer Demo.");

        Logger logger = LoggerFactory.getLogger(ProducerDemoWithCallback.class);

        //create Producer propertie

        Properties  properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"127.0.0.1:9092");
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,StringSerializer.class.getName());

        //create the producer
        KafkaProducer<String,String> producer = new KafkaProducer<String, String>(properties);

        //create a producer record


        for(int i=3;i>0;i--) {

            ProducerRecord<String, String> record = new ProducerRecord<>("first_topic", i+" bałwanków było w jednym lesie, ni mniej ni więcej tylko właśnie "+i);

            //send data
            producer.send(record, new Callback() {
                @Override
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    //executes every time record is succesfully sent or an exception is thrown
                    if (e == null) {
                        //record sent ok
                        logger.info("Received new metadata: \n" +
                                "\nTopic: " + recordMetadata.topic() +
                                "\nPartition: "+ recordMetadata.partition() +
                                "\nOffset: "+recordMetadata.offset() +
                                "\n Timestamp: "+recordMetadata.timestamp()

                        );


                    } else {
                        logger.error("Error when producing: "+e);
                    }
                }
            });
        }




        producer.flush();
        producer.close();


    }
}
